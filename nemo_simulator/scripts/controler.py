#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Twist


def move():
    # Starts a new node and a publisher that sends the robot's velocity data in the topic '/cmd_vel'
    rospy.init_node('move', anonymous=True)
    velocity_publisher = rospy.Publisher('/cmd_vel', Twist, queue_size=10)
    vel_msg = Twist()


    #linear and angular velocity vectors
    vel_msg.linear.x = 1
    vel_msg.linear.y = 0
    vel_msg.linear.z = 0
    vel_msg.angular.x = 0
    vel_msg.angular.y = 0
    vel_msg.angular.z = 0

    while not rospy.is_shutdown(): #making a loop to move the robot
        
        velocity_publisher.publish(vel_msg)


if __name__ == '__main__':
    try:
        move()
    except rospy.ROSInterruptException:
        pass #this allows the code to be stopped when we press Ctrl-C in the terminal